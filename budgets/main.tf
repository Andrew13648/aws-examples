provider "aws" {
  region = "us-east-1"
}

variable email_addresses {
  type = list(string)
}

variable daily_limit_amount {
  type = number
}

resource "aws_budgets_budget" "daily_budget" {
  name              = "daily-budget"
  budget_type       = "COST"
  limit_amount      = var.daily_limit_amount
  limit_unit        = "USD"
  time_unit         = "DAILY"

  notification {
    comparison_operator        = "GREATER_THAN"
    threshold                  = 20
    threshold_type             = "PERCENTAGE"
    notification_type          = "ACTUAL"
    subscriber_email_addresses = var.email_addresses
  }
}
