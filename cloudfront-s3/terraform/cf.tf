locals {
  s3_origin_id = aws_s3_bucket.bucket.bucket
}

resource "aws_cloudfront_origin_access_identity" "sample_identity" {
  comment = "access-identity-${aws_s3_bucket.bucket.bucket_regional_domain_name}"
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.bucket.bucket_regional_domain_name
    origin_id   = local.s3_origin_id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.sample_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "S3 Distribution"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    compress = true
    viewer_protocol_policy = "allow-all"
    # TTL in seconds
    min_ttl                = 0
    default_ttl            = 60
    max_ttl                = 60
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

output "distribution_url" {
  value = "https://${aws_cloudfront_distribution.s3_distribution.domain_name}"
}
