provider "aws" {
  region = "us-east-1"
}

variable "account_number" {
  type = string
}

data "aws_iam_policy_document" "sample_policy" {
  statement {
    actions = ["sts:*"]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "assume_role_policy" {
  # Allow anything in the account to assume this role
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = [var.account_number]
    }
  }
}

resource aws_iam_policy "sample_policy" {
  name = "sample-policy"
  policy = data.aws_iam_policy_document.sample_policy.json
}

resource "aws_iam_role" "sample_role" {
  name               = "sample-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource aws_iam_role_policy_attachment "attach_sample_policy" {
  policy_arn = aws_iam_policy.sample_policy.arn
  role       = aws_iam_role.sample_role.name
}